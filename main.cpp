#include <iostream>
#include <regex>
#include <fstream>
#include <string>
using namespace std;
int main() {

  std::ifstream ifs("sample.in");
  std::string var((std::istreambuf_iterator<char>(ifs)),
                  (std::istreambuf_iterator<char>()));

  const regex r("answer=\"(.+?)\"");
  smatch sm;

  auto params_it = std::sregex_iterator(var.cbegin(), var.cend(), r);
  auto params_end = std::sregex_iterator();

  while (params_it != params_end) {
    auto param = params_it->str();
    std::regex_match(param, sm, r);
    std::cout << sm[1] << std::endl;
    ++params_it;
  }

  return 0;
}
